
const Error404 = () => {
    return (
        <>
        <h1>La pagina a la que intentas ingresar no se encuentra disponible</h1>
        </>
    )
};

export default Error404;