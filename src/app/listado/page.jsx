"use client";
import React, { useEffect, useState } from "react";
import styles from "@/styles/Home.module.css";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { Backdrop, Checkbox, CircularProgress, Dialog, DialogActions, DialogContent, DialogTitle, IconButton, TextField, Tooltip } from "@mui/material";
import Divider from "@mui/material/Divider";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import { useAppDispatch, useAppSelector } from "@/redux/hooks";
import { addNewTask, createTask } from "@/redux/states/task.state";
import { useRouter } from "next/navigation";

export default function Listado() {
  const router = useRouter();
  const fechaActual = new Date();
  const opciones = {
    weekday: "long",
    year: "numeric",
    month: "short",
    day: "numeric",
  };
  const taskState = useAppSelector((state) => state.taskReducer);
  const userState = useAppSelector((state) => state.userReducer);
  const dispatch = useAppDispatch();
  const [states, setStates] = useState({
    nameTask: "",
    completedTask: false,
    id: 0,
    disabledAdd: true,
    idEdit: 0,
    titleEdit: '',
    openModalEdit: false,
    open: false,
  });

  const handleNameTask = (e) => {
    setStates({
      ...states,
      nameTask: e.target.value,
    });
  };

  const handleAdd = (e) => {
    setStates({
      ...states,
      open: true,
    });
    const taskNew = {
      id: new Date().getTime(),
      title: states.nameTask,
      completed: false,
    };
    if (taskState && taskState.length > 0) {
      dispatch(addNewTask(taskNew));
      setStates({
        ...states,
        open: false,
      });
    } else {
      dispatch(createTask([taskNew]));
      setStates({
        ...states,
        open: false,
      });
    }
    setStates({
      ...states,
      id: 0,
      nameTask: "",
    });
  };

  const handleDeleteTask = async (id) => {
    const newTaskState = taskState.filter((obj) => obj.id !== id);
    dispatch(createTask(newTaskState));
  };

  const handleEditTask = (t) => {
    setStates({
      ...states,
      idEdit: t.id,
      titleEdit: t.title,
      openModalEdit: true,
    });
  };

  const handleClose = () => {
    setStates({
      ...states,
      openModalEdit: false,
    });
  };

  const handleChangeEditTask = (e) => {
      setStates({
        ...states,
        titleEdit: e.target.value,
      });
  };

  const handleClickEditTask = async () => {
    const newTask = taskState.map((task) => {
      if (task.id === states.idEdit) {
        return {
          ...task,
          title: states.titleEdit,
        };
      }
      return task;
    });
    dispatch(createTask(newTask));
    setStates({
      ...states,
      idEdit:0,
      titleEdit:'',
      openModalEdit: false,
    });
  };

  useEffect(() => {
    if (states.nameTask !== "") {
      setStates({
        ...states,
        disabledAdd: false,
      });
    } else {
      setStates({
        ...states,
        disabledAdd: true,
      });
    }
  }, [states.nameTask]);

  useEffect(() => {
    if(!userState.permission){
      router.push('/error404');
    }
  }, [userState.permission]);

  return (
    <>
    <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={states.open}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <h2 className={styles.h2}>Mi día</h2>
      <Typography
        sx={{ fontSize: 16, marginTop: "5px" }}
        color="text.secondary"
        gutterBottom
      >
        {fechaActual.toLocaleDateString("es-es", opciones)}
      </Typography>
      <Card sx={{ minWidth: "35vw", display: "flex" }}>
        <CardContent>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Nombre de tarea nueva"
            type="text"
            fullWidth
            color="warning"
            variant="standard"
            value={states.nameTask}
            onChange={(e) => handleNameTask(e)}
          />
        </CardContent>
        <CardActions
        >
          <Button
            onClick={handleAdd}
            color="warning"
            size="small"
            disabled={states.disabledAdd}
          >
            Añadir tarea
          </Button>
        </CardActions>
      </Card>
      <Divider sx={{ margin: "10px" }} />
      {taskState &&
        taskState.length > 0 &&
        taskState.map((task) => {
          return (
            <Card
              key={task.id}
              sx={{
                minWidth: "80vw",
                display: "flex",
                justifyContent: "space-between",
                marginBottom: "8px",
              }}
            >
              <CardContent>
                <Tooltip title="Tarea Completada" placement="top-start">
                  <Checkbox color="warning" />
                </Tooltip>
              </CardContent>
              <CardContent>
                <Typography
                  sx={{ fontSize: 14, marginTop: "10px" }}
                  color="text.secondary"
                  gutterBottom
                >
                  {task.title}
                </Typography>
              </CardContent>
              <CardActions
                anchorOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
              >
                <Tooltip title="Editar tarea" placement="top-start">
                  <IconButton
                      onClick={() => handleEditTask(task)}
                    aria-label="edit"
                  >
                    <EditIcon />
                  </IconButton>
                </Tooltip>
                <Tooltip title="Eliminar tarea" placement="top-start">
                  <IconButton
                    onClick={() => handleDeleteTask(task.id)}
                    aria-label="delete"
                  >
                    <DeleteIcon />
                  </IconButton>
                </Tooltip>
              </CardActions>
            </Card>
          );
        })}
        <Dialog
        open={states.openModalEdit}
        onClose={handleClose}
        sx={{
          "& .MuiPaper-root": {
            background: "#FFEA98",
          },
        }}
      >
        <DialogTitle>Editar Post</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Nombre tarea"
            type="text"
            fullWidth
            color="warning"
            variant="outlined"
            value={states.titleEdit}
            onChange={(e) => handleChangeEditTask(e)}
          />
        </DialogContent>
        <DialogActions>
          <Button color="warning" onClick={handleClose}>
            Cancelar
          </Button>
          <Button color="warning" onClick={handleClickEditTask}>
            Editar
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}
