"use client";
import Image from "next/image";
import {
  Backdrop,
  Button,
  Card,
  CardContent,
  CardMedia,
  CircularProgress,
  TextField,
  Typography,
} from "@mui/material";
import styles from "@/styles/Home.module.css";
import React, { useEffect, useState, useContext, useRef } from "react";
import { useAppDispatch, useAppSelector } from "@/redux/hooks";
import { createUser } from "@/redux/states/user.state";
import { useRouter } from "next/navigation";
import service from "@/services/service";

const LOGIN_URL = "https://apimocha.com/taskfig/auth";

export default function Home() {
  const router = useRouter();
  const dispatch = useAppDispatch();

  const [states, setStates] = useState({
    errorMs: "",
    userName: "",
    password: "",
    buttonDisabled: true,
    open: false,
  });

  const handleChangeInputs = (e, type) => {
    if (type === "userName") {
      setStates({
        ...states,
        userName: e.target.value,
      });
    }
    if (type === "password") {
      setStates({
        ...states,
        password: e.target.value,
      });
    }
  };

  const handleLogin = async () => {
    setStates({
      ...states,
      open: true,
    });
    try {
      const newUserState = {
        userName: states.userName,
        password: states.password,
        permission: true,
      };
      const response = await service.post(LOGIN_URL, newUserState);
      if (response?.status === 200) {
        dispatch(createUser(newUserState));
        router.push("/listado");
        setStates({
          ...states,
          open: false,
        });
      } else {
        setStates({
          ...states,
          errorMs: "Usuario o contraseña invalidos",
          open: false,
        });
      }
    } catch (error) {
      setStates({
        ...states,
        errorMs: "Usuario o contraseña invalidos",
        open: false,
      });
    }
  };

  useEffect(() => {
    if (states.userName !== "" && states.password !== "") {
      setStates({
        ...states,
        buttonDisabled: false,
      });
    } else {
      setStates({
        ...states,
        buttonDisabled: true,
      });
      const newUserState = {
        userName: '',
        password: '',
        permission: false,
      };
      dispatch(createUser(newUserState));
    }
  }, [states.userName, states.password]);

  return (
    <>
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={states.open}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <div>
        <h1 className={styles.h1}>Bienvenido a TaskFig</h1>
      </div>
      <Card sx={{ maxWidth: 600, background: "#FEDD63" }}>
        <CardContent>
          <Typography
            gutterBottom
            variant="h5"
            component="div"
            className={styles.text1}
          >
            Iniciar Sesión
          </Typography>
          {states.errorMs && (
            <Typography
              sx={{ color: "red", fontSize: "14px" }}
              gutterBottom
              component="div"
            >
              Usuario o contraseña incorrectos
            </Typography>
          )}
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="UserName"
            type="text"
            fullWidth
            color="warning"
            variant="outlined"
            value={states.userName}
            onChange={(e) => handleChangeInputs(e, "userName")}
            required
          />
          <TextField
            autoFocus
            margin="dense"
            id="pass"
            label="Contraseña"
            type="password"
            fullWidth
            color="warning"
            variant="outlined"
            value={states.password}
            onChange={(e) => handleChangeInputs(e, "password")}
            required
          />
          <Button
            color="warning"
            variant="outlined"
            disabled={states.buttonDisabled}
            onClick={handleLogin}
          >
            Iniciar sesión
          </Button>
        </CardContent>
      </Card>
    </>
  );
}
