
import { createSlice } from "@reduxjs/toolkit";

export const TaskEmptyState= [
  {
    id: 0,
    title: '',
    completed: false,
  },
];

export const taskSlice = createSlice({
  name: "task",
  initialState: [],
  reducers: {
    createTask: (state, action) => action.payload,
    modifyTask: (state, action) => ({ ...state, ...action.payload }),
    addTask(state, action) {
      state.push(action.payload);
    },
    addNewTask(state, action) {
      state.push(action.payload);
    },
    resetTask: () => TaskEmptyState,
  },
});

export const {
  createTask,
  modifyTask,
  resetTask,
  addTask,
  addNewTask,
} = taskSlice.actions;

export default taskSlice.reducer;