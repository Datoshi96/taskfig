
import { createSlice } from "@reduxjs/toolkit";

export const FilterEmptyState = [
  {
    id: 0,
    title: '',
    body: '',
  },
];

export const filterSlice = createSlice({
  name: "filter",
  initialState: {},
  reducers: {
    createFilter: (state, action) => action.payload,
    modifyFilter: (state, action) => ({ ...state, ...action.payload }),
    addFilter(state, action) {
      state.push(action.payload);
    },
    resetFilter: () => FilterEmptyState,
  },
});

export const {
  createFilter,
  modifyFilter,
  resetFilter,
} = filterSlice.actions;

export default filterSlice.reducer;