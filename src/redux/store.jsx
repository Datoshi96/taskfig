import { configureStore } from '@reduxjs/toolkit';
import { taskSlice } from './states/task.state';
import { userSlice } from './states/user.state';
import taskReducer from './states/task.state';
import { pageSlice } from './states/page.state';
import pageReducer from './states/page.state';
import userReducer from './states/user.state'
import filterReducer from './states/filter.state'

export const store = configureStore({
  reducer:{
    taskReducer,
    pageReducer,
    userReducer,
    filterReducer,
  }
});

export const makeStore = () => {
  return configureStore({
    reducer: {
      task: taskSlice.reducer,
      user: userSlice.reducer,
      page: pageSlice.reducer,
    }
  })
}